@extends('layouts.info')

@section('content')

<style>
	.table-info-us tr
		{
			-webkit-transition: background-color .5s ease-in-out;
			-moz-transition: background-color .5s ease-in-out;
			-o-transition: background-color .5s ease-in-out;
			transition: background-color .5s ease-in-out;
		}

	.table-info-us tr:hover td
		{
			cursor: pointer;
			background-color: #b0d9e6;
		}
		
	.table-info-detail .v-head
		{
			color: #ffffff;
			background-color: #63a4b8;
		}
		
		.info-table 
		{
			display: none;
		}
		
	.table-info-detail
		{
			display:none;
		}
</style>

<script>
$(document).ready(function(){
	
	$('.table-info-us tr').click(function(){
		$.ajax({
			url: '/test-job/getinfo',
			type: 'GET',
			dataType: "json"
		}).done(function(data){
			$('.info-table').show();
			$('.table-info-detail .icon-td').html('');
			$('.table-info-detail .date-td').html(data.date);
			$('.table-info-detail .name-td').html(data.name);
			$('.table-info-detail .phone-td').html(data.phone);
			$('.table-info-detail .e-mail-td').html(data.email);
			$('.table-info-detail .radio-td').html(data.label);
			$('.table-info-detail .checkbox-td').html(data.label);
			$('.table-info-detail').show();
		})
	});
	
});
</script>

        <!-- Page Content -->
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Info about current user</h1>
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover table-info-us">
								<thead>
								<tr>
									<th>Icon</th>
									<th>Date</th>
									<th>Name</th>
									<th>Phone</th>
									<th>E-mail</th>
								</tr>
								</thead>
								<tbody>
								<tr>
								
									<td></td>
								
									@foreach ($user_info as $value)
										<td> {{ $value }} </td>
									@endforeach
									
								</tr>
								</tbody>
							</table>
						</div>
						
						<h2 class="page-header info-table">Detail info user</h2>
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover table-info-detail">
								<tbody>
								<tr>
								
									<td class="v-head">icon</td>
									<td class="icon-td">Icon</td>
									
								</tr>
								<tr>
								
									<td class="v-head">Date</td>
									<td class="date-td">date</td>
								
								</tr>
								<tr>
								
									<td class="v-head">Name</td>
									<td class="name-td">name</td>
								
								</tr>
								<tr>
								
									<td class="v-head">phone</td>
									<td class="phone-td">phone</td>
								
								</tr>
								<tr>
								
									<td class="v-head">E-mail</td>
									<td class="e-mail-td">e-mail</td>
								
								</tr>
								<tr>
								
									<td class="v-head">Radio</td>
									<td class="radio-td">radio</td>
								
								</tr>
								<tr>
								
									<td class="v-head">Checkbox</td>
									<td class="checkbox-td">Checkbox</td>
								
								</tr>
								</tbody>
							</table>
						</div>
						
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
            </div>
@endsection