<?php

namespace App\Http\Controllers;

use App\Models\Lead;
use App\Models\Customer;
use App\Models\SphereAttr;
use App\Models\SphereAttrOptions;
use App\Models\SphereMask;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
//use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Sentinel;


class TestjobController extends BaseController
{
	
	public function index(){
		
		$curr_user = Sentinel::getUser();
		$user_info = Lead::select('leads.date', 'leads.name', 'customers.phone', 'leads.email')
			  ->join('customers', 'leads.customer_id', '=', 'customers.id')
			  ->join('open_leads', 'leads.id', '=', 'open_leads.lead_id')
			  ->where('leads.agent_id', $curr_user->id)
			  ->where('open_leads.agent_id', $curr_user->id)
			  ->first();

		return view('views.page.testjob')
		->with('user_info', $user_info->toArray());
	}
	
	public function detailInfo() {
		$curr_user = Sentinel::getUser();
		
		$leads_id = Lead::where('agent_id', $curr_user->id)->first();
		$l_sid = (int)$leads_id->sphere_id;
		$sphere_mask = new SphereMask($l_sid);
		
		$aid = SphereAttr::where('sphere_id', $leads_id->sphere_id)->first();
		$oid = SphereAttrOptions::where('sphere_attr_id', $aid->id)->first();
		$btid = $sphere_mask->where('fb_'.$aid->id.'_'.$oid->id, '1')
				->where('user_id', $curr_user->id)
				->first();
				
		$user_info = Lead::select('leads.date', 'leads.name', 'customers.phone', 'leads.email', 'sphere_attributes.label')
			  ->join('customers', 'leads.customer_id', '=', 'customers.id')
			  ->join('open_leads', 'leads.id', '=', 'open_leads.lead_id')
			  ->join('sphere_attributes', 'leads.sphere_id', '=', 'sphere_attributes.sphere_id')
			  ->join('sphere_attribute_options', 'sphere_attributes.id', '=', 'sphere_attribute_options.sphere_attr_id')
			  ->join('sphere_bitmask_'.$leads_id->sphere_id, 'leads.id', '=', 'sphere_bitmask_'.$leads_id->sphere_id.'.user_id')
			  ->where('leads.agent_id', $curr_user->id)
			  ->where('open_leads.agent_id', $curr_user->id)
			  ->where('sphere_attribute_options.ctype', 'agent')
			  ->where('sphere_bitmask_'.$leads_id->sphere_id.'.type', 'lead')
			  ->first();
			  
			  echo json_encode($user_info->toArray());
	}

}
